#!/usr/bin/python
# -*- coding: utf-8 -*-

# dic = {1: 'a', 2: 'b', 3: 'c'}
# a = []
# for i in dic:
#     a.append(dic.get(i))
# print(a)

# import os
#
# for a in os.walk('.'):
#     print(a[0],a[1],a[2])
#
# for root, dirs, files in os.walk('.'):
#     print(root)
#     print(dirs)
#     print(files)
#     print("============")

# import re
#
#
# def add1(match):
#     val = match.group()
#     num = int(val) + 1
#     return str(num)
#
#
# str3 = 'imooc videonum=1000'
# result = re.sub(r'\d+', add1(), str3)
# print(result)

# def f(i, *args):
#     print(i, args)
#
#
# t = (4, 5, 6)
# d = {'a': 7, 'b': 8, 'c': 9}
# f(*t)
# f(1, 2, *t)
# f(q="winning", **d)
# f(1, 2, *t, q="winning", **d)
# (1,2,4,5,6){}

# A0 = dict(zip(('a', 'b', 'c', 'd', 'e'), (1, 2, 3, 4, 5)))
# A1 = range(10)
# A2 = sorted([i for i in A1 if i in A0])
# A3 = sorted([A0[s] for s in A0])
# A4 = [i for i in A1 if i in A3]
# A5 = {i: i * i for i in A1}
# A6 = [[i, i * i] for i in A1]
# A7 = [x * 2 for x in range(10)]
# print(A6)




# class A(object):
#     def __init__(self):
#         print("enter A")
#         super(A, self).__init__()
#         print("leave A")
#
#
# class B(object):
#     def __init__(self):
#         print("enter B")
#         super(B, self).__init__()
#         print("leave B")
#
#
# class C(A):
#     def __init__(self):
#         print("enter C")
#         super(C, self).__init__()
#         print("leave C")
#
#
#
# class D(A):
#     def __init__(self):
#         print("enter D")
#         super(D, self).__init__()
#         print("leave D")
#
#
#
# class E(B, C):
#     def __init__(self):
#         print("enter E")
#         super(E, self).__init__()
#         print("leave E")
#
#
#
# class F(E, D):
#     def __init__(self):
#         print("enter F")
#         super(F, self).__init__()
#         print("leave F")
#
# f = F()

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class A(object):
    def go(self):
        print("go A go!")

    def stop(self):
        print("stop A stop!")

    def pause(self):
        raise Exception("Not Implemented")


class B(A):
    def go(self):
        super(B, self).go()
        print("go B go!")


class C(A):
    def go(self):
        super(C, self).go()
        print("go C go!")

    def stop(self):
        super(C, self).stop()
        print("stop C stop!")

    def pause(self):
        super(C, self).pause()
        print("wait D wait!")


class D(B, C):
    def go(self):
        super(D, self).go()
        print("go D go!")

    def stop(self):
        super(D, self).stop()
        print("stop D stop!")

    def pause(self):
        print("wait D wait!")


class E(B, C):
    pass


a = A()
b = B()
c = C()
d = D()
e = E()

# a.go()
# b.go()
# c.go()
# d.go()
# e.go()
# a.stop()
# b.stop()
# c.stop()
# d.stop()
# e.stop()
# a.pause()
# b.pause()
# c.pause()
# d.pause()
# e.pause()

dup_list = [1, 2, 3, 4, 4, 4, 5, 1, 2, 7, 8, 8, 10]
expect_list = []
# for element in dup_list:
#     if element not in expect_list:
#         expect_list.append(element)
# # print(expect_list)


# a = [3, 4, 6, 10, 11, 18]
# b = [1, 5, 7, 12, 13, 19, 21]
# a.extend(b)
# print(sorted(a))

# def f(x):
#     for i in range(x):
#         i = i**3
#         yield i
#
# for x in f(5):
#     print(x)

# ss = "I figured It out i figured it out from black and write seconds AND hours maybe the had to take some time"
# sslist = ss.split(' ')
# print(sslist)
#
# # Method 1~~~~~~~~~~~
# count = 0
# for i in sslist:
#     sslist[count] = i.lower()
#     count += 1
# print(sslist)
# print(sslist.count('i'))
#
#
# # Method 2~~~~~~~~~~~~
# def func(x):
#     count = 0
#     for i in sslist:
#         if i.lower() == x.lower():
#             count += 1
#     print("The %s word Appeared time is %d" % (x, count))
# func('I')

import re

# sentence = "Sometimes (when I nest them (my parentheticals) too much (like this (and this))) they get confusing."
# lst = sentence.split(' ')
# # lst = re.split(r' |(|)', sentence)
# # print(lst)
# leftlst = []
# # print(len(sentence))
# llll = 'I (want (to be) (a supper) (hero))'
#
#
# def func(string, i):
#     while i < len(string):
#         if string[i] == '(':
#             leftlst.append(string[i])
#         elif string[i] == ')':
#             leftlst.pop()
#             if len(leftlst) == 0:
#                 print("The position is %d" % i)
#                 return
#         i += 1
#
#
# func(sentence, 10)

# class Bird:
#     def __init__(self):
#         self.hungry = True
#         self.sound = 'quawk!'
#         print('b')
#
#     def eat(self):
#         if self.hungry:
#             print('Aaaaa...')
#             self.hungry = False
#         else:
#             print('No,thanks!')
#
#
#
# class SongBird(Bird):
#     def __init__(self):
#         super(SongBird, self).__init__()
#         self.sound = 'Squawk!'
#         print('a')
#
#     def sing(self):
#         print(self.sound)
#
#     def eat(self):
#         if self.hungry:
#             print('Baaa...')
#             self.hungry = False
#         else:
#             print('thanks!')
# sb=SongBird()
# sb.sing()
# sb.__init__()
# # sb.eat()
# # sb.eat()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# array = [[1, 4, 6], [6, 3, 7], [3, 3, 3]]
# lst = []
# array_dup = [[] for i in range(3)]
#
#
# def Dereplication(array):
#     index = 0
#     for i in array:
#         for j in i:
#             if j not in lst:
#                 lst.append(j)
#                 array_dup[index].append(j)
#             else:
#                 continue
#         index += 1
#     print(lst)
#     print(array_dup)
#
#
# Dereplication(array)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#
# import threading
# import time
#
#
# class myThread(threading.Thread):  # 继承父类threading.Thread
#     def __init__(self, threadID, name, counter):
#         threading.Thread.__init__(self)
#         self.threadID = threadID
#         self.name = name
#         self.counter = counter
#
#     def run(self):
#         print("Starting " + self.name)
#         # 获得锁，成功获得锁定后返回True
#         # 可选的timeout参数不填时将一直阻塞直到获得锁定
#         # 否则超时后将返回False
#         threadLock.acquire()
#         print_time(self.name, self.counter, 3)
#         # 释放锁
#         threadLock.release()
#
# def print_time(threadName, delay, counter):
#     while counter:
#         time.sleep(delay)
#         print("%s: %s" % (threadName, time.ctime(time.time())))
#         counter -= 1
#
# threadLock = threading.Lock()
# threads = []
#
#
# # 创建新线程
# thread1 = myThread(1, "Thread-1", 1)
# thread2 = myThread(2, "Thread-2", 2)
#
# # 开启线程
# thread1.start()
# thread2.start()
#
# # 添加线程到线程列表
# threads.append(thread1)
# threads.append(thread2)
# print(threads)
#
# # 等待所有线程完成
# for t in threads:
#     t.join()
# print(t)
# print("Exiting Main Thread")
# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#

# def fibs(x):
#     result = [0, 1]
#     for i in range(x-2):
#         result.append(result[-2] + result[-1])
#     print(result)
#
# if __name__ == '__main__':
#     num = input("Enter the number:")
#     fibs(int(num))

# import random
#
# print(random.choice(range(10)))
# print(random.uniform(1, 10))
# print(random.randint(1, 10))
# lst = ['a', 'b', 'c', 'd']


# lst.reverse()
# try:
#     for i in lst:
#         print(i)
# finally:
#     lst.reverse()
#     print(lst)
#
# for i in range(len(lst) - 1, -1, -1):
#     x = lst[i]
#     print(x)

# import re
# s = '<html><head><title>Title</title>'
# print(re.match('<.*?>', s))

# a = ((2, 3), 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19)
#
#
# # a = tuple(a)
# # print(a)
#
#
# def to_list(t):
#     return [i for i in t if not isinstance(i, tuple)]
# a = to_list(a)
# print(a)
import copy

# a = [1, 2, 3]
# c = {1: 'a', 2: 'b'}
# b = copy.copy(a)
# d = c.copy()
# print(b)
# print(d)

a = [[10], 20]
# a.append(21)
# e = copy.copy(a)
e = copy.copy(a)
f = copy.deepcopy(a)
a[0].append(11)

# a[].append(3)
# print(e)
# print(f)
# print(a)

# roman = {'M': 1000, 'D': 500, 'C': 100, 'L': 50, 'X': 10, 'V': 5, 'I': 1}
# s = 'XCIX'
# prev = roman[s[len(s) - 1]]
# print(prev)

lst = [3, 4, 3, 3, 3, 2, 1, 1, 6, 4, 5, 3]

# def Func(nums):
#     for i in range(0, len(nums)):
#         for j in range(1, len(nums) - 1):
#             if nums[i] == nums[j]:
#                 nums.pop(j)
#
#     return nums
#
# print(Func(lst))
#
# def Func(nums):
#     i = 0
#     while i < len(nums):
#         j = i + 1
#         while j < len(nums):
#             if nums[i] == nums[j]:
#                 nums.pop(j)
#                 j -= 1
#             j += 1
#         i += 1
#
#     return nums
#
#
# print(Func(lst))

# str = ''
# for i in range(len(str)):
#     print(i)

# a = [1, 2, 3]
# index = 11
# b = dict((key, []) for key in a)
#
#
# def Func(value):
#     for i in range(1, len(a) + 1):
#         for j in range(0, value + 1, len(a)):
#             b[i].append(i + j)
#
#     b_keys =list(b.keys())
#     b_values = list(b.values())
#
#     for i in range(len(b_values)):
#         if value in b_values[i]:
#             return b_keys[i]
#
#
# result = Func(index)
# print(result)


# a = [1, 2, 3]
# index_min = -1
# index = -3


# def FuncAnother(value, index_min):
#     index = (value - index_min) % 3
#     return a[index]

# def FuncAnother(value, index_min):
#     if index_min >= 0:
#         index = (value - index_min) % 3
#         return a[index]
#     else:
#         index = -(value - index_min) % 3
#         return a[index]
#
#
# print(FuncAnother(index, index_min))
# print(9)
import datetime
import time

now = datetime.datetime.now()
print(now)

oneday_input = datetime.datetime(1988, 5, 3)
print(oneday_input)

dayss = (now - oneday_input).days / 365
print(dayss)
#
# dayss = datetime.timedelta(days=1000)
# print(dayss)

# days_1 = now - dayss
# print(days_1)

