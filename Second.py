#!/usr/bin/python
# -*- coding: utf-8 -*-

# strings = ['xxxxd', 'ccc', 'ddd']
# index = 0
# for string in strings:
#         if 'xxx' in string:
#                 strings[index] = 'censored'
#         index += 1
# print strings
#
# for index, string in enumerate(strings):
#         if 'xxx' in string:
#                 strings[index] = 'censored'
# print strings
#

# from math import sqrt
# for n in range(99, 0, -1):
#         root = sqrt(n)
#         if root == int(root):
#                 print n
#                 break

# word = 'dummy'
#
# word = raw_input('Please enter a word:')
# while word:
#         print 'The word was ' + word
#         # word = raw_input('Please enter a word:')

# from math import sqrt
# for n in range(90, 81, -1):
#         root = sqrt(n)
#         if root == int(root):
#                 print n
#                 break
# else:
#     print "Didn't find it!"

# girls = ['alice', 'bernice', 'clarice']
# boys = ['chris', 'arnold', 'bob']
# LetterGirls = {}
# for girl in girls:
#         LetterGirls.setdefault(girl[0], []).append(girl)
#         # print LetterGirls.setdefault(girl[0], [])
#         # print LetterGirls
# print [b+'+'+g for b in boys for g in LetterGirls[b[0]]]
# print [b+'+'+g for b in boys for g in girls if b[0] == g[0]]

# for number in range(1, 101):
#         print number

# def fibs(num):
#     result = [0, 1]
#     for i in range(num-2):
#         result.append(result[-2] + result[-1])
#     return result
# print fibs(10)


# def test():
#     return 'This is printed'
# x = test()
# print x

# ~~~~~~~~~~~~~~P95
# storage = {}
# storage['first'] = {}
# storage['middle'] = {}
# storage['last'] = {}
# me = 'Magnus Lie Hetland'
# storage['first']['Magnus'] = [me]
# storage['middle']['Lie'] = [me]
# storage['last']['Hetland'] = [me]
# print storage['middle']['Lie']
# print storage
#
# my_sister = 'Anne Lie Hetland'
# storage['first'].setdefault('Anne', []).append(my_sister)
# storage['middle'].setdefault('Lie', []).append(my_sister)
# storage['last'].setdefault('Hetland', []).append(my_sister)
# print storage['first']['Anne']
# print storage['middle']['Lie']
# print storage

#
def init(data):
    data['first'] = {}
    data['middle'] = {}
    data['last'] = {}


# # #
# init(storage)
# print storage
# #
def lookup(data, label, name):
    return data[label].get(name)


# # print lookup(storage, 'middle', 'Lie')
# # print storage
# # data = {'middle': {'Lie': ['Magnus Lie Hetland']}, 'last': {'Hetland': ['Magnus Lie Hetland']}, 'first': {'Magnus': ['Magnus Lie Hetland']}}
#
# # ~~~~~~~~~~p96
def store(data, full_name):
    names = full_name.split()
    if len(names) == 2:
        names.insert(1, '')
    labels = 'first', 'middle', 'last'
    for label, name in zip(labels, names):
        people = lookup(data, label, name)
        if people:
            # people.append(full_name)
            # print people
            pass
        else:
            data[label][name] = [full_name]


mynames = {}
init(mynames)
# mynames = {'middle': {'yong': ['Wu yong jie']}, 'last': {}, 'first': {}}
mynames = {'middle': {'yong': ['Wang yong jia']}, 'last': {'jia': ['Wang yong jia']},
           'first': {'Wang': ['Wang yong jia']}}
print(store(mynames, 'Wang yong jia'))
# print store(mynames, 'Wu yong jie')
# print store(mynames, 'Tian ye peng')

print('~~~~~~~')
print(lookup(mynames, 'middle', 'ye'))
store(mynames, 'Tian ye peng')
print(lookup(mynames, 'middle', 'ye'))
print(mynames)
print('+++++++++++++++')


# init(mynames)
# print mynames
# store(mynames, 'Magnus Lie Hetland')
# store(mynames, 'Wu yong jie')
# print mynames
# print lookup(mynames, 'middle', 'yong')
# print mynames

# ~~~~~~~~~~~~
#
# def inc(x):
#     x[0] = x[0] + 1
#     print x[0]
# foo = [10]
# inc(foo)
# print foo
# #~~~~~~~~~~~~~~erro
# def foo(x, y, z, m=0, n=0):
#     print x, y, z, m, n
# def call_foo(*args, **kwds):
#     print "Calling foo!"
#     foo(*args, **kwds)
#
# print foo
# print call_foo
# ~~~~~~~~~~~~~~~~~
#
# def story(**kwds):
#     return 'Once upon a time, there was a'\
#             '%(job)s called &(name)s.' %kwds
#
# def power(x, y, *others):
#     if others:
#         print 'Received redundant parameters:' ,others
#     return pow(x, y)
#
# def interval(start, stop=None, step=1):
#     'Imitates range() for step >0'
#     if stop is None:
#         start, stop=0, start
#     result = []
#     i = start
#     while i < stop:
#         result.append(i)
#         i += step
#     return result
# ~~~~~~~~~~~~~~~
# x=1
# def change_global():
#     global x
#     x=x+1
#     return x
# print change_global()
#
# ~~~~~~~~~~~~~~~
# def multiplier(factor):
#     def multiplyByfactor(number):
#         return number*factor
#     return multiplyByfactor
#
# double = multiplier(2)
# print double(5)
#
# def func():
#     print "Button function"
#
# def click(fn):
#     fn()
#     print "Clicked!"
#
# def complexFunc(number):
#     print "Button with number",number
#
# click(func)
# # click((lambda: complexFunc(9)))
# num=9
# foo=(lambda: complexFunc(num))
# click(foo)
#
# def normalFunc(number):
#    return number+num
#
# print normalFunc(5)
# num=1
# print normalFunc(5)
#
# def func(a):
#     if a > 100:
#         return True
#     else:
#         return False
# print filter(func,[10,56,101,500])
# ~~~~~~~~~~~~~
# def fibs(num):
#     result = [0,1]
#     for i in range(num-2):
#         result.append(result[-2] + result[-1])
#     return result
#
# print fibs(10)

# ~~~~~~~~~~~~~~~~
# def changeInt(x):
#     x=9
#     return x
#
# i=1
# print i
# print changeInt(i)
#
# def change(x):
#     x[0]=99
#     return x
#
# a=[1,2,3,4]
# print a
# print change(a)
# print a
#
# def change_two(x):
#     x=[100,99]
#     return x
#
# a=[1,2,3,4]
# print '================='
# print a
# print change_two(a)
# print a
# a=change_two(a)
# print a

# ~~~~~~~~~~~~~~
# __metaclass__ = type
#
# class person:
#
#     def setName(self, name):
#         self.name = name
#     def getName(self):
#         return self.name
#     def greet(self):
#         print "Hello, world! I'm %s." % self.name
#
# foo = person()
# bar = person()
# foo.setName('wu yongjie')
# print foo.getName()
# bar.setName('tian yepeng')
# print bar.getName()

# # foo.greet()
# # bar.greet()
# # print foo.name
# # bar.name = 'Yoda'
# # print bar.name
# # bar.greet()

# ~~~~~~~~~~~~
# class MemberCounter:
#     members = 0
#     def init(self):
#         MemberCounter.members += 1
# m1 = MemberCounter()
# m1.init()
# print MemberCounter.members
# m2 = MemberCounter()
# m2.init()
# print MemberCounter.members
# MemberCounter.members = 100
# print m1.members, m2.members
# m1.members = 'Ada'
# print m1.members, m2.members
# # ~~~~~~~~~~
# class Filter:
#     def init(self):
#         self.blocked = []
#     def filter(self, sequence):
#         return [x for x in sequence if x not in self.blocked]
# class SPAMFilter(Filter):
#     def init(self):
#         self.blocked = ['SPAM']
# s = SPAMFilter()
# s.init()
# print s.filter(['SPAM', 'A', 'B', 'C', 'SPAM'])
# print SPAMFilter.__bases__, Filter.__bases__
# ~~~~~~~~~~~~~~

class Calculator:
    def calculate(self, expression):
        self.value = eval(expression)


class Talker:
    def talk(self):
        print('Hi, my value is', self.value)

    def rename(self, name):
        self.name = name
        print('OK')


class TalkingCalculator(Calculator, Talker):
    pass


tc = TalkingCalculator()
# tc.calculate('1+2*3')
# tc.talk()
tc.rename('B')


# ~~~~~~~~~~~~~~~~~~

# gName='111'
#
# class OpenObject:
#     gName = 'Lisa'
#     def setName(self, name):
#         global gName
#         gName = name
#
#     def getName(self):
#         global gName
#         return gName
#
#
# o = OpenObject()
#
# print gName,1
# print OpenObject.gName,2
# print o.gName,20
# print o.getName(),3
# gName = 'Ada'
# print o.getName()
#
# p = OpenObject()
# o.setName('wu yongjie')
# print p.getName()
# print gName
# print OpenObject.gName
# print '=============='
# ~~~~~~~~~~~~~~~~~
# class MuffledCalculator:
#     muffled = False
#     def calc(self, expr):
#         try:
#             return eval(expr)
#         except ZeroDivisionError:
#             if self.muffled:
#                 print 'Division by zero is illegal'
#             else:
#                 raise
#
# calculator = MuffledCalculator()
# print calculator.calc('10/2')
# # print calculator.calc('10/0')
# calculator.muffled = True
# calculator.calc('10/0')
# ~~~~~~~~~~~~~~~~~~
# while True:
#     try:
#         x = input('Enter the first number:')
#         y = input('Enter the second number:')
#         print x/y
#         # value = x/y
#         # print 'x/y is', value
#
#     # except:
#     #     print 'Invalid input. please try again.'
#     # else:
#     #     break
#
#     # except (ZeroDivisionError, TypeError, NameError), e:
#     #     print e
#
#     except ZeroDivisionError:
#         print "A"
#     # except TypeError:
#         # print "B"
#     # except NameError:
#     #     print "C"
# ~~~~~~~~~~~~~~~~~
# print '**********'
# # x = None
# try:
#     x = 1/0
# except NameError:
#     print 'Unknown variable'
# else:
#     print 'That went well!'
# finally:
#     print 'Cleaning up.'
#     del x
# ~~~~~~~~~~~~~~~~
# print '#################'
#
# class SomeCustomException(Exception):
#     pass
#
# def faulty():
#     raise ZeroDivisionError('Something is wrong')
#
# def ignore_exception():
#     faulty()
#
# def handle_exception():
#     try:
#         a=1
#         b=2
#         # raise SomeCustomException('someee')
#         faulty()
#         c=3
#         print 'End try'
#     except IndexError:
#         print 'lalala'
#     except ZeroDivisionError:
#         print 'Zero catched!'
#     except SomeCustomException:
#         print 'Custom !! Exception'
#     except:
#         print 'Exception handled'
#
# # print ignore_exception()
# print handle_exception()
# print 'END'
# ~~~~~~~~~~~~~~~~~~~~~~~~~
# person = {'name': 'Leaf', 'age': 42}
#
# def desribePerson(person):
#     print 'Description of', person['name']
#     print 'Age:', person['age']
#     try:
#         print 'Occupation: ' + person['occupation']
#     except KeyError:
#         pass
#
#
# desribePerson(person)
#
# try:
#     tc.value
# except AttributeError:
#     print "The person is not est"
# else:
#     print "The person is exist"
#
# print getattr(tc, 'name')
#
# print callable(getattr(tc, 'talk'))
#
# try:
#     exception=ZeroDivisionError()
#     raise exception
#     raise ZeroDivisionError()
#     raise ZeroDivisionError
# except ZeroDivisionError:
#     print 'Catched!'
# ~~~~~~~~~~~~~~~~~
# __metaclass__ = type
#
# class Bird:
#     def __init__(self):
#         self.hungry = True
#     def eat(self):
#         if self.hungry:
#             print 'Aaaaah...'
#             self.hungry = False
#         else:
#             print 'No, thanks!'
#
#
# class SongBird(Bird):
#     def __init__(self):
#         # Bird.__init__(self)
#         super(SongBird, self).__init__()
#         self.sound = 'Squawk!'
#     def sing(self):
#         print self.sound
# sb = SongBird()
# sb.sing()
# ~~~~~~~~~~~~~~~~~~~~~~
__metaclass__ = type

def checkIndex(key):
    if not isinstance(key, int):
        raise TypeError
    if key < 0:
        raise IndexError


class ArithmeticSequence:
    def __init__(self, start=0, step=1):
        self.start = start
        self.step = step
        self.changed = {}

    def __getitem__(self, key):
        checkIndex(key)
        try:
            return self.changed[key]
        except KeyError:
            return self.start + key * self.step

    def __setitem__(self, key, value):
        checkIndex(key)
        self.chenged[key] = value


s = ArithmeticSequence(1, 2)
print(s)
print(s[4])
# s[4] = 2
print(s[5])
# ~~~~~~~~~~~~~~~~~~~~~
# class FooBar:
#     def __init__(self, value = 42):
#         self.somevar = value
#
# f = FooBar()
# print f.somevar
# ~~~~~~~~~~~~~~~~~~~~~~~

# class Rectangle:
#     def __init__(self):
#         self.width = 0
#         self.height = 0
#
#     def setSize(self, size):
#         self.width, self.height = size
#
#     def getSize(self):
#         return self.width, self.height
#
#
# r = Rectangle()
# r.width = 10
# r.height = 5
# print r.getSize()
# print r.setSize((88, 77))
# print r.width, r.height
#
# __metaclass__ = type
#
#
# class Rectangleproperty:
#     def __init__(self):
#         self.width = 0
#         self.height = 0
#
#     def setSize(self, size):
#         self.width, self.height = size
#
#     def getSize(self):
#         return self.width, self.height
#
#     size = property(getSize, setSize)
#
#
# t = Rectangleproperty()
# t.width = 10
# t.height = 5
# print t.size
# t.size = 66, 44
# print r.width
#
# print '======================'
#
# class Rectangleattr:
#     def __init__(self):
#         self.width = 0
#         self.height = 0
#         self.a = 0
#
#     def __setattr__(self, name, value):
#         if name == 'size':
#             self.width, self.height = value
#         elif name == 'width' or name == 'height':
#             self.__dict__[name] = value
#         elif name == 'a':
#             self.width = self.width
#         else:
#             # if(name=='asd'):
#             self.a = 33
#             # self.__dict__[name] = value
#
#     def __getattr__(self, name):
#         if name == 'size':
#             # return self.width, self.height
#             return self.b
#         else:
#             raise AttributeError
#
# # 类的实例化时调用init
# y = Rectangleattr()
# y.width = 10
# y.height = 5
# print getattr(y, 'size')
# setattr(y, 'size', (88, 99))
# print y.width, y.height
# print getattr(y, 'size')
# print '*******'
# print getattr(y, 'width')
#
# # setattr(y,'asd',(8,9))
# # y.asd=(8,9)
# # print y.asd
# # print getattr(y,'asd')
#
# print y.asd
# print '==================='
# ~~~~~~~~~~~~~~~~~~~~~~~

# class Fibs:
#     def __init__(self):
#         self.a = 0
#         self.b = 1
#     def next(self):
#         self.a, self.b = self.b, self.a+self.b
#         return self.a
#     def __iter__(self):
#         return self
#
# fibs = Fibs()
# for f in fibs:
#     if f > 1000:
#         print f
#         break
# ~~~~~~~~~~~~~~~~~~~~~~~

# class TestIterator:
#     value = 0
#     def next(self):
#         self.value += 1
#         if self.value > 10:
#             raise StopIteration
#         return self.value
#     def __iter__(self):
#         return self
#
# ti = TestIterator()
# print ti.next()
# print list(ti)

# print ti.next()

# ~~~~~~~~~~~~~~~~~~~~~~~~~
# def flatten(nested):
#     for sublist in nested:
#         for element in sublist:
#             yield element
#
#
#
# nested = [[1, 2], [3, 4], [5]]
# for num in flatten(nested):
#     print num
# # print flatten2(nested)
# # print flatten2(nested)
# # a = flatten(nested)
# # a.next()
# # a.next()
# # a.next()
# print list(flatten(nested))

# def flatten3(nested):
#     try:
#         for sublist in nested:
#             for element in flatten3(sublist):
#                 yield element
#     except TypeError:
#         yield nested
#
# print list(flatten3([[[1],2],3,4,[5,[6,7]],8]))
#
#
# def flatten4(nested):
#     try:
#         try:
#             nested + ''
#         except TypeError:
#             pass
#         else:
#             raise TypeError
#
#         for sublist in nested:
#             for element in flatten4(sublist):
#                 yield element
#     except TypeError:
#         yield nested
#
# print list(flatten4(['foo', ['bar', ['baz']]]))
#
# name = ['A', 'B', 'C']
# def test():
#     for i in name: #range(99,108):
#         yield i
#
# # p=test()
# # for i in range(0,9):
# #     print p.next()
# #
# # for i in 'asdf':
# #     print i
#
# print '!!!!!!!!!!'
# for i in test():
#     print i
#
# print '!!!!!!!!!!'
#
# print '&&&&&&&&&&&&&&&&'
# for i in test():
#     print i
# print '&&&&&&&&&&&&&&&&'
# p=test()
# # while(True):
# #     try:
# #         i=p.next()
# #         print i
# #     except StopIteration:
# #         pass
#
#
# print '$$$$$$$$'
# for i in range(1, 10):
#     print i
# ~~~~~~~~~~~~~~~~~~
class CounterList(list):
    def __init__(self, *args):
        super(CounterList, self).__init__(*args)
        self.counter = 0

    def __getitem__(self, index):
        self.counter += 1
        return super(CounterList, self).__getitem__(index)

c1 = CounterList(list(range(10)))
print(c1)
c1.reverse()
print(c1)
del c1[3: 6]
print(c1)
print(c1.counter)
print(c1[4] + c1[2])
print(c1.counter)
#~~~~~~~~~~~~~~~~
#numberlines.py


# import fileinput
#
# for line in fileinput.input(inplace=True):
#     line = line.rstrip()
#     num = fileinput.lineo()
#     print '%-40s # %2i' % (line, num)
#~~~~~~~~~~~~~~~~~~~
# file=open("somefile.txt","w")
# try:
#     # Write data to your file.
#     file.write("lalala")
# finally:
#     file.close()
#
# with open("somefile.txt","w") as file:
#     # Write data to your file.
#     file.write("lalala")
# ~~~~~~~~~~~~~~~~~~
# def process(string):
#     print 'Processing: ', string


# f = open("somefile.txt")
# char = f.read(1)
# while char:
#     process(char)
#     char = f.read(1)
# f.close()

# f = open("somefile.txt")
# while True:
#     char = f.read(1)
#     if not char:
#         break
#     process(char)
# f.close()

# f = open('somefile.txt')
# while True:
#     line = f.readline()
#     if not line:
#         break
#     process(line)
# f.close()

# f = open('somefile.txt')
# for char in f.read():
#     process(char)
# f.close()

# f = open('somefile.txt')
# for line in f.readlines():
#     process(line)
# f.close()

# import fileinput
# for line in fileinput.input('somefile.txt'):
#     process(line)
# print fileinput.lineno()
# print fileinput.filename()
# print fileinput.filelineno()

# for line in open('somefile.txt'):
#     process(line)

# import sys
# for line in sys.stdin:
#     process(line)

# f = file('somefile.txt')
# print f
# f = open('somefile.txt')
# print f



