# s = "Hello World"
s = "b a"


class Solution(object):
    def lengthOfLastWord(self, s):
        """
        :type s: str
        :rtype: int
        """
        word_count = 0

        if not s:
            return 0

        for i in range(len(s) - 1, -1, -1):
            if s[i] == ' ' and word_count == 0:
                continue
            if s[i] == ' ' and word_count != 0:
                break
            word_count += 1
        return word_count


sol = Solution()
result = sol.lengthOfLastWord(s)
print(result)
