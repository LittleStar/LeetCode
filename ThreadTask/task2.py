from Tkinter import *
import time, threading

master = Tk()

# chkboxVar = IntVar()
# chkboxVar.set(1)
# Checkbutton(master, text="male", variable=chkboxVar).grid(row=0, sticky=W)

w = Canvas(master, width=800, height=600)
w.pack()

w.create_line(0, 0, 200, 100)
w.create_line(0, 100, 200, 0, fill="red", dash=(4, 4))

rect = w.create_rectangle(50, 25, 150, 75, fill="blue")

response = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

status_green = 0
status_red = 0
is_green = True
delay_number = 0.1


class myThread(threading.Thread):
    def __init__(self, name, delay):
        super(myThread, self).__init__()
        self.name = name
        self.delay = delay

    def run(self):
        global response
        while 1:
            print('Start:' + self.name)
            process(self.delay, response)
            print('End:' + self.name)


def process(delay, lst):
    global status_green, status_red
    global is_green
    index = 0
    current = 1

    for number in lst:
        lock.acquire()
        if index == 0:
            if number != current:
                status_red = time.time()
            else:
                status_green = time.time()

        if index > 0:
            if number == 0:
                if number != current:
                    status_red = time.time()
                    if abs(status_red - status_green) > 1:
                        current = 0
                        is_green = False
                else:
                    status_red = time.time()

            if number == 1:
                if number != current:
                    status_green = time.time()
                    if abs(status_green - status_red) > 1:
                        current = 1
                        is_green = True
                    else:
                        status_green = time.time()

        lock.release()
        index += 1
        time.sleep(delay)



class guiThread(threading.Thread):
    def __init__(self):
        super(guiThread, self).__init__()

    def run(self):
        while 1:
            if is_green is True:
                w.itemconfig(rect, fill="green")
                time.sleep(0.1)
            else:
                w.itemconfig(rect, fill="red")
                time.sleep(0.1)


thread1 = myThread("Thread-1", delay_number)
thread2 = guiThread()

lock = threading.Lock()

thread1.start()
thread2.start()

mainloop()
print("Exit")
