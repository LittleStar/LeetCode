from Tkinter import *
import GUI
import time
import threading

response = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]


is_green = True
delay_number = 0.1
lock = threading.Lock()


class myThread(threading.Thread):
    def __init__(self, name, delay):
        super(myThread, self).__init__()
        self.name = name
        self.delay = delay
        self.status_green = None
        self.status_red = None

    def run(self):
        global response
        while 1:
            print('Start:' + self.name)
            self.process(self.delay, response)
            print('End:' + self.name)

    def process(self, delay, lst):
        global is_green
        index = 0
        current = 1

        self.status_red = time.time()
        self.status_green = time.time()

        for number in lst:
            lock.acquire()
            if number == 0:
                self.status_red = time.time()
                if number != current:
                    if abs(self.status_red - self.status_green) > 1:
                        current = 0
                        is_green = False

            if number == 1:
                self.status_green = time.time()
                if number != current:
                    if abs(self.status_green - self.status_red) > 1:
                        current = 1
                        is_green = True

            lock.release()
            index += 1
            time.sleep(delay)


class guiThread(threading.Thread):
    def __init__(self):
        super(guiThread, self).__init__()
        self.gui = GUI.GreenRedStatus()

    def run(self):
        while 1:
            self.gui.color_process(is_green)
            time.sleep(0.1)


thread1 = myThread("Thread-1", delay_number)
thread2 = guiThread()


thread1.start()
thread2.start()

mainloop()

print("Exit")
