# import threading
# import time
#
# cents = 0
#
# class Tt(threading.Thread):
#     def __init__(self, value):
#         super(Tt, self).__init__()
#         self.value = value
#
#     def run(self):
#         global cents
#         while 1:
#             lock.acquire()  # 不加锁会产生竞争
#             cents = self.value
#             if cents == self.value:
#                 # print('Right')
#                 pass
#                 # self.value=cents
#             else:
#                 print('Wrong',cents)
#             lock.release()
#
#
#
# thread1 = Tt(1)
# thread2 = Tt(9)
# lock = threading.Lock()
#
# thread1.start()
# thread2.start()
#
# while 1:
#     # print(cents)
#     pass


""" Dead lock """
import threading

cents = 0


class Tt(threading.Thread):
    def __init__(self, value):
        super(Tt, self).__init__()
        self.value = value

    def run(self):
        global cents
        while 1:
            lock.acquire()
            cents = self.value
            if cents == self.value:
                another_lock.acquire()  # 会产生死锁
                print('Right', self.value)
                another_lock.release()  # 会产生死锁
                # pass
            else:
                print('Wrong', cents)
            lock.release()


class Att(threading.Thread):
    def __init__(self, value):
        super(Att, self).__init__()
        self.value = value

    def run(self):
        global cents
        while 1:
            another_lock.acquire()
            cents = self.value
            if cents == self.value:
                lock.acquire()  # 会产生死锁
                print('Right', self.value)
                lock.release()  # 会产生死锁
                # pass
            else:
                print('Wrong', cents)
            another_lock.release()


thread1 = Tt(1)
thread2 = Att(2)
lock = threading.Lock()
another_lock = threading.Lock()

thread1.start()
thread2.start()

while 1:
    # print(cents)
    pass
