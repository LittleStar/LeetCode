from Tkinter import *
import time, threading

master = Tk()

# chkboxVar = IntVar()
# chkboxVar.set(1)
# Checkbutton(master, text="male", variable=chkboxVar).grid(row=0, sticky=W)

w = Canvas(master, width=800, height=600)
w.pack()

w.create_line(0, 0, 200, 100)
w.create_line(0, 100, 200, 0, fill="red", dash=(4, 4))

rect = w.create_rectangle(50, 25, 150, 75, fill="blue")

# response = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
#             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
#             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]

response = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

# response = [1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,1 ,1]

status_i = 0
status_j = 0
isgreen = True
delay_number = 0.1


class myThread(threading.Thread):
    def __init__(self, name, delay):
        super(myThread, self).__init__()
        self.name = name
        self.delay = delay

    def run(self):
        global response
        while 1:
            print('Start:' + self.name)
            process(self.delay, response)
            print('End:' + self.name)



def process(delay, lst):
    global status_i, status_j
    global isgreen
    index = 0
    for number in lst:
        lock.acquire()
        if index == 0:
            if number == 0:
                status_i = time.time()
            if number == 1:
                status_j = time.time()

        if index > 0:
            if number == 0:
                if lst[index - 1] == number:
                    status_j = time.time()
                    if abs(status_j - status_i) > 1:
                        isgreen = False
                else:
                    status_i = time.time()

            if number == 1:
                if lst[index - 1] == number:
                    status_i = time.time()
                    if abs(status_j - status_i) > 1:
                        isgreen = True
                else:
                    status_j = time.time()

        lock.release()
        index += 1
        time.sleep(delay)


class guiThread(threading.Thread):
    def __init__(self):
        super(guiThread, self).__init__()

    def run(self):
        while 1:
            if isgreen is True:
                w.itemconfig(rect, fill="green")
                time.sleep(0.1)
            else:
                w.itemconfig(rect, fill="red")
                time.sleep(0.1)


thread1 = myThread("Thread-1", delay_number)
thread2 = guiThread()

lock = threading.Lock()

thread1.start()
thread2.start()

mainloop()
print("Exit")
