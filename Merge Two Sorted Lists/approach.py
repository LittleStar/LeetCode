# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution(object):
    def mergeTwoLists(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """

        temp = cur = ListNode(0)
        while l1 and l2:
            if l1.val < l2.val:
                cur.next = l1
                l1 = l1.next
            else:
                cur.next = l2
                l2 = l2.next
            cur = cur.next
        cur.next = l1 or l2
        return temp.next


list1_head = ListNode(1)
a = list1_head
a.next = ListNode(3)
a = a.next
a.next = ListNode(5)

list2_head = ListNode(2)
a = list2_head
a.next = ListNode(4)
a = a.next
a.next = ListNode(6)

sol = Solution()
result = sol.mergeTwoLists(list1_head, list2_head)
print(result)


def iter(linklist):
    yield linklist.val
    while linklist.next:
        linklist = linklist.next
        yield linklist.val


for node in iter(result):
    print(node)
