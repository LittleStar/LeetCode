# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution(object):
    def __init__(self):
        self.head = None

    def mergeTwoLists(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """

        self.head = cur = ListNode(0)
        while l1 and l2:
            if l1.val < l2.val:
                cur.next = l1
                l1 = l1.next
            else:
                cur.next = l2
                l2 = l2.next
            cur = cur.next
        cur.next = l1 or l2
        self.head = self.head.next
        return self.head

    def iter(self):
        temp = self.head
        yield temp.val
        while temp.next:
            temp = temp.next
            yield temp.val


list1_head = ListNode(1)
a = list1_head
a.next = ListNode(3)
a = a.next
a.next = ListNode(5)

list2_head = ListNode(2)
a = list2_head
a.next = ListNode(4)
a = a.next
a.next = ListNode(6)

sol = Solution()
sol.mergeTwoLists(list1_head, list2_head)
for node in sol.iter():
    print(node)
