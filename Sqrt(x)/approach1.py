x = 2147483647


# x=82

class Solution(object):
    def mySqrt(self, x):
        """
        :type x: int
        :rtype: int
        """

        # Approach 1
        # for i in range(1, (x + 1)/2):
        #     if i * i >= x:
        #         return i - 1


        # Approach 2
        # r = x
        # while r * r > x:
        #     r = (r + x / r) / 2
        #     print(r)
        # return r

        # Approach 3
        return int(x ** 0.5)


sol = Solution()
result = sol.mySqrt(x)
print(result)
