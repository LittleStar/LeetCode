
sentence = '[['

class Solution(object):
    def isValid(self, s):
        """
        :type s: str
        :rtype: bool
        """
        p_dic = {')': '(', ']': '[', '}': '{'}
        p_lst = list(p_dic.items())
        leftlst = []

        for element in s:

            if element == p_lst[0][1] or element == p_lst[1][1] or element == p_lst[2][1]:
                leftlst.append(element)

            elif element == p_lst[0][0] or element == p_lst[1][0] or element == p_lst[2][0]:
                if len(leftlst) == 0:
                    return False
                elif p_dic[element] == leftlst[-1]:
                    leftlst.pop()
                else:
                    return False

        if len(leftlst) > 0:
            return False

        return True


l = Solution()
print(l.isValid(sentence))
