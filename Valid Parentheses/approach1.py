sentence = '['


class Solution(object):
    def isValid(self, s):
        """
        :type s: str
        :rtype: bool
        """
        p_dic = {'(': ')', '[': ']', '{': '}'}
        leftlst = []

        for element in s:
            if element in p_dic:
                leftlst.append(element)
            elif len(leftlst) > 0 and element == p_dic[leftlst[-1]]:
                leftlst.pop()
            else:
                return False

        if len(leftlst) == 0:
            return True
        else:
            return False


l = Solution()
print(l.isValid(sentence))
