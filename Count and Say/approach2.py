number = 5


class Solution(object):
    def countAndSay(self, n):
        """
        :type n: int
        :rtype: str
        """

        cs_str = '1'

        for i in range(n - 1):
            element, temp, count = cs_str[0], '', 0
            for j in cs_str:
                if element == j:
                    count += 1
                else:
                    temp += str(count) + element
                    element = j
                    count = 1
            temp += str(count) + element
            cs_str = temp
        return cs_str


sol = Solution()
ll = sol.countAndSay(number)
print(ll)
