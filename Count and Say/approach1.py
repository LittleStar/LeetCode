#!/usr/bin/python
# -*- coding: utf-8 -*-

number = 5
import re


class Solution(object):
    def countAndSay(self, n):
        """
        :type n: int
        :rtype: str
        """
        s = '1'
        for _ in range(n - 1):
            s = re.sub(r'(.)\1*', lambda m: str(len(m.group())) + m.group(1), s)
        # (.)匹配任一字符并分组   \1匹配第一个分组的内容  *匹配前一个字符0次或无限次

        # s = re.sub(r'(.)\1*', lambda m: str(len(m.group())) + m.group(1), '1211')
        # s = re.search(r'(.)\1*', '2111221')
        # s = re.search(r'(.)\1(33)\2', '12233331')
        # s = re.sub(r'(\b[a-z]+) \1', r'\1', 'cat in the the!hat')

        return s


sol = Solution()
ll = sol.countAndSay(number)
print(ll)



# ~~~~~~~~~~~~~~~~
# for i in range(n-1):
#
#     if i == 0 and n == 1:
#         result = cs_str
#         return result
#
#     elif i > 0:
#         count = 1
#         if len(cs_str) == 1:
#             count = 1
#             element = cs_str[0]
#             result += str(count) + element
#             cs_str = result
#
#         result = ''
#         for j in range(len(cs_str)-1):
#             k = j + 1
#             if cs_str[j] == cs_str[k]:
#                 count += 1
#                 element = cs_str[j]
#                 result = str(count) + element
#                 cs_str = result
#
#             else:
#                 count = 1
#                 element = cs_str[j]
#                 element2 = cs_str[k]
#                 result = str(count) + element + str(count) + element2
#                 # result += str(count) + element
#
#                 cs_str = result
#
# return result
