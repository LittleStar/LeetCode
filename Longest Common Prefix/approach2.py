# str = ['abchgfd', 'abchth']
str = ["aca","cba"]


class Solution(object):
    def longestCommonPrefix(self, strs):
        """
        :type strs: List[str]
        :rtype: str
        """
        common_prefix = ''
        i = 0
        j = 0

        if len(strs) == 0:
            return ''
        if len(strs) == 1:
            return strs[i]

        for i in range(len(strs) - 1):
            temp = ''
            if i == 0:
                min_len = min(len(strs[i]), len(strs[i + 1]))
            else:
                min_len = min(len(common_prefix), len(strs[i + 1]))

            if min_len == 0:
                return ''

            for j in range(min_len):

                if i == 0:
                    if strs[i][0] != strs[i+1][0]:
                        return ''

                    elif strs[i][j] == strs[i + 1][j]:
                        common_prefix += strs[i][j]
                        j += 1
                        continue

                else:
                    if j == 0 and common_prefix[j] != strs[i + 1][j]:
                        return ''
                    else:
                        if common_prefix[j] == strs[i + 1][j]:
                            temp += common_prefix[j]

                            if j == min_len - 1:
                                common_prefix = temp
                            j += 1
                        else:
                            common_prefix = temp
                            break
            i += 1

        return common_prefix


l = Solution()
common_prefix = l.longestCommonPrefix(str)
print(common_prefix)
