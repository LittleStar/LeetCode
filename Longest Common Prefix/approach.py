# str = ['abchgfd', 'abchth']
str = []


class Solution(object):
    def longestCommonPrefix(self, strs):
        """
        :type strs: List[str]
        :rtype: str
        """
        strs.sort()

        if not strs:
            return ''

        a = strs[0]
        b = strs[-1]
        min_len = min(len(a), len(b))
        index = 0

        for i in range(min_len):
            if a[i] == b[i]:
                index += 1
            else:
                break

        return a[:index]


l = Solution()
common_prefix = l.longestCommonPrefix(str)
print(common_prefix)
