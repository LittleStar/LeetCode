a = '11'
b = '11'


class Solution(object):
    def addBinary(self, a, b):
        """
        :type a: str
        :type b: str
        :rtype: str
        """
        i = 0
        carry = '0'
        result = ''

        while i < max(len(a), len(b)) or carry == '1':
            if i < len(a):
                str_a = a[-1 - i]
            else:
                str_a = '0'

            if i < len(b):
                str_b = b[-1 - i]
            else:
                str_b = '0'

            value = int(str_a) + int(str_b) + int(carry)
            result = str(value % 2) + result

            if value > 1:
                carry = '1'
            else:
                carry = '0'

            i += 1
        return result


sol = Solution()
result = sol.addBinary(a, b)
print(result)
