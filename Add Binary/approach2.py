a = '11'
b = '1'


class Solution(object):
    def addBinary(self, a, b):
        """
        :type a: str
        :type b: str
        :rtype: str
        """
        # return bin(eval('0b' + a) + eval('0b' + b))[2:]
        #      or
        return '{:b}'.format(int(a, 2) + int(b, 2))


sol = Solution()
result = sol.addBinary(a, b)
print(result)
