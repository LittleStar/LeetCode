x = 0


class Solution(object):
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        str_x = str(x)
        l = len(str_x)

        result = True
        i = 0

        while i < l // 2:
            j = l - i - 1
            if str_x[i] == str_x[j]:
                i += 1
                continue
            else:
                result = False
                return result

        return result

l = Solution()
result = l.isPalindrome(x)
print(result)