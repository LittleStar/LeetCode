str = 'a'
str2 = ''

# str='' str2='' return 0
# str='a' str2='' return 0
# str='' str2='a' return -1


class Solution(object):
    def strStr(self, haystack, needle):
        """
        :type haystack: str
        :type needle: str
        :rtype: int
        """

        l1 = len(haystack)
        l2 = len(needle)

        if l2 > l1:
            return -1
        elif l2 == 0:
            return 0

        l = l1 - l2
        for i in range(l + 1):
            if haystack[i:i + l2] == needle:
                return i

        return -1


sol = Solution()
result = sol.strStr(str, str2)
print(result)

# ~~~~~~~~~~~~There may be mistake~~~~~~~~~~~~~~~~~~~~~~~~~

# class Solution(object):
#     def strStr(self, haystack, needle):
#         """
#         :type haystack: str
#         :type needle: str
#         :rtype: int
#         """
#
#         if haystack == needle == '':
#             return 0
#
#         if not haystack:
#             return -1
#
#         if len(needle) > len(haystack):
#             return -1
#
#         j = 0
#
#         for i in range(len(haystack)):
#             if not needle:
#                 return 0
#             t = i
#             while t < len(haystack) and needle[j] == haystack[t]:
#                 j += 1
#                 if j == len(needle):
#                     if j != 1:
#                         t = t - j + 1
#                     return t
#                 t += 1
#                 continue
#
#             j = 0
#
#         return -1

