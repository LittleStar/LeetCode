lst = [1, 1, 1, 1, 1, 2]


class Solution(object):
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """

        if not nums:
            return 0

        i = 0
        for j in range(1, len(nums)):
            if nums[i] == nums[j]:
                continue
            else:
                i += 1
                nums[i] = nums[j]
        return i + 1


sol = Solution()
result = sol.removeDuplicates(lst)
print(result)
