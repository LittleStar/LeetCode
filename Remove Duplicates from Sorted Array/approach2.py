lst = [-1,-1]


class Solution(object):
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """

        if not nums:
            return 0

        i = 0
        while i < len(nums):
            j = i + 1
            while j < len(nums):
                if nums[i] == nums[j]:
                    nums.pop(j)
                    j -= 1
                j += 1
            i += 1
        return nums


sol = Solution()
result = sol.removeDuplicates(lst)
print(result)
