lst = [-2, 1, -3, 4, -1, 2, 1, -5, 4]

class Solution(object):
    def maxSubArray(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        l = len(nums)
        # max_number = nums[l-1]
        # cmp_number = nums[l-1]

        # for i in range(l - 2, -1, -1):
        #     if nums[i] > cmp_number + nums[i]:
        #         cmp_number = nums[i]
        #     else:
        #         cmp_number = cmp_number + nums[i]
        #
        #     if cmp_number > max_number:
        #         max_number = cmp_number
        #
        # return max_number
        # ~~~~~~~~~~
        # max_number = nums[0]
        # cmp_number = nums[0]
        #
        # for i in range(1, l):
        #     if nums[i] > cmp_number + nums[i]:
        #         cmp_number = nums[i]
        #     else:
        #         cmp_number = cmp_number + nums[i]
        #
        #     if cmp_number > max_number:
        #         max_number = cmp_number
        #
        # return max_number
#         ~~~~~~~~~~

        max_number = nums[0]
        cmp_number = nums[0]

        for num in nums[1:]:
            if num > cmp_number + num:
                cmp_number = num
            else:
                cmp_number = cmp_number + num

            if cmp_number > max_number:
                max_number = cmp_number

        return max_number


sol = Solution()
result = sol.maxSubArray(lst)
print(result)
