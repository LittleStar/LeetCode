lst = [-2, 1, -3, 4,2, -1, 2, 1, -5, 4]


class Solution(object):
    def maxSubArray(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """

        max_lst = []
        l = len(nums)
        j = 0
        for i in range(l - 1, -1, -1):
            if i == l - 1:
                max_lst.append(nums[i])

            else:
                if nums[i] > max_lst[j] + nums[i]:
                    max_lst.append(nums[i])
                else:
                    max_lst.append(max_lst[j] + nums[i])
                j += 1
        # max_lst.pop(0)
        return max(max_lst)


sol = Solution()
result = sol.maxSubArray(lst)
print(result)
