lst = [5, 3, 3, 7, 2,3,3,6,7,6,9,3]
number = 3


class Solution(object):
    def removeElement(self, nums, val):
        """
        :type nums: List[int]
        :type val: int
        :rtype: int
        """
        if not nums:
            return 0

        i = 0
        for j in range(0, len(nums)):
            if nums[j] == val:
                continue
            else:
                nums[i] = nums[j]
                i += 1
        return i

sol = Solution()
result = sol.removeElement(lst, number)
print(result)
