lst = [1,3,5]
number = 2


class Solution(object):
    def searchInsert(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        return len([x for x in nums if x < target])


sol = Solution()
result = sol.searchInsert(lst, number)
print(result)