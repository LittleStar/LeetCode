digits = [7, 8, 9]
number = 234


class Solution(object):
    def plusOne(self, digits, number):
        """
        :type digits: List[int]
        :rtype: List[int]
        """
        for i in range(len(digits) - 1, -1, -1):
            if i == len(digits) - 1:
                counter = str(digits[-1] + number)
            else:
                counter = str(digits[i] + int(counter))

            l = len(counter)
            if l == 1:
                digits[i] = int(counter)
                break
            else:
                digits[i] = int(counter[-1])
                counter = str(counter[:-1])
                if i == 0 and l == 2:
                    digits.insert(0, int(counter[0]))

        return digits


sol = Solution()
result = sol.plusOne(digits, number)
print(result)
