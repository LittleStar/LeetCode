digits = [9, 9, 9, 9]


class Solution(object):
    def plusOne(self, digits):
        """
        :type digits: List[int]
        :rtype: List[int]
        """
        sumnumber = str(digits[-1] + 1)

        for i in range(len(digits) - 1, -1, -1):
            l = len(sumnumber)
            if l == 1:
                digits[i] = int(sumnumber)
                break
            else:
                sumnumber = str(int(digits[i]) + int(sumnumber[0]))
                l = len(sumnumber)
                if l == 1:
                    digits[i] = int(sumnumber)
                    break
                digits[i] = int(sumnumber[1])
                if i == 0 and l == 2:
                    digits.insert(0, int(sumnumber[0]))

        return digits

        #         ~~~~~~~~~~~~~~~~~~~~
        # for i in range(len(digits) - 1, -1, -1):
        #     if digits[i] < 9:
        #         digits[i] += 1
        #         return digits
        #     else:
        #         digits[i] = 0
        # digits.insert(0, 1)
        # return digits


sol = Solution()
result = sol.plusOne(digits)
print(result)
