#!/usr/bin/python
# -*- coding: utf-8 -*-


x = 2147483647
y = -123


class Solution(object):
    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        positive_max = 2 ** 31 - 1
        negative_min = -(2 ** 31)
        result = 0

    # Method1
    # str_x = str(x)
    # str_reverse = str_x[::-1]
    #
    # if str_reverse[-1] == '-':
    #     int_reverse = int(str_reverse.strip('-'))
    #     if -int_reverse < negative_min:
    #         return 0
    #     else:
    #         result = -int_reverse
    # else:
    #     if int(str_reverse) > positive_max:
    #         return 0
    #     else:
    #         result = int(str_reverse)
    #
    # return result


        # Method2
        if x > 0:
            int_x = int(str(x)[::-1])
        else:
            int_x = -int(str(-x)[::-1])

        if (int_x > negative_min) and (int_x < positive_max):
            return int_x
        else:
            return 0


l = Solution()
result = l.reverse(1534236469)
print(result)
