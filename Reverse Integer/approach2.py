#!/usr/bin/python
# -*- coding: utf-8 -*-

import operator

x = 2147483640
y = -10


class Solution(object):
    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        positive_max = str(2 ** 31 - 1)
        l = len(positive_max)
        negative_min = str(-(2 ** 31)).strip('-')

        result = None

        str_x = str(x)
        str_reverse = str_x[::-1]

        if str_reverse[-1] == '-':
            str_reverse = str_reverse.strip('-')
            str_len = len(str_reverse)

            if str_len == l:
                i = 0
                for element in str_reverse:
                    if element > negative_min[i]:
                        return 0
                    elif element == negative_min[i]:
                        i += 1
                        continue
                    else:
                        result = -int(str_reverse)
                        break

            else:
                result = -int(str_reverse)

        else:
            str_len = len(str_reverse)
            if str_len == l:
                i = 0
                for element in str_reverse:
                    if element > positive_max[i]:
                        return 0
                    elif element == positive_max[i]:
                        i += 1
                        continue
                    else:
                        result = int(str_reverse)
                        break
            else:
                result = int(str_reverse)

        return result


l = Solution()
result = l.reverse(1563847412)
print(result)

