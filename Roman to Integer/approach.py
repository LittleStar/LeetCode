s = 'XCIX'


class Solution(object):
    def romanToInt(self, s):
        """
        :type s: str
        :rtype: int
        """
        # lst = ['I', 'V', 'X', 'L', 'C', 'D', 'M']
        dic_s = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}
        lst_num = []
        i = 0
        result = 0

        for element in s:
            value = dic_s[element]
            lst_num.append(value)

        while i < len(lst_num):
            current = int(lst_num[i])
            if i == len(lst_num) - 1 or lst_num[i] >= lst_num[i + 1]:
                result += current


            else:
                result -= current

            i += 1

        return result



l = Solution()
lst_num = l.romanToInt(s)
print(lst_num)
