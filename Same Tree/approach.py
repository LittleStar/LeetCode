class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Solution(object):
    # def __init__(self):
    #     self.root = None

    def isSameTree(self, p, q):
        """
        :type p: TreeNode
        :type q: TreeNode
        :rtype: bool
        """
        # self.root = TreeNode(0)
        if not p and not q:
            return True
        elif not p or not q:
            return False
        elif p.val != q.val:
            return False
        else:
            return self.isSameTree(p.left, q.left) and self.isSameTree(p.right, q.right)

    def myiter(self, p):
        cur = p
        yield cur.val
        if cur.left:
            for val in self.myiter(cur.left):
                yield val
        if cur.right:
            for val in self.myiter(cur.right):
                yield val

            # def myiter(self, p):
            #     cur = p
            #     yield cur.val
            #     while cur.left or cur.right:
            #         yield self.myiter(cur.left) and self.myiter(cur.right)




tree1 = TreeNode(1)
a = tree1
a.left = TreeNode(2)
a.right = TreeNode(3)
a.left.left = TreeNode(10)
a.left.right = TreeNode(5)
a.right.left = TreeNode(6)
a.right.right = TreeNode(7)

tree2 = TreeNode(1)
b = tree2
b.left = TreeNode(2)
b.right = TreeNode(3)
b.left.left = TreeNode(4)
b.left.right = TreeNode(5)
b.right.left = TreeNode(6)
b.right.right = TreeNode(7)

print(a)
print(b)

sol = Solution()
sol.isSameTree(tree1, tree2)
print("*******")

for node in sol.myiter(tree1):
    print(node)
print("!!!!!!!!")

for node in sol.myiter(tree2):
    print(node)
print("!!!!!!!!")
