class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution(object):
    def __init__(self):
        self.head = None

    def deleteDuplicates(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """

        cur = self.head = head
        while cur and cur.next:
            if cur.next.val == cur.val:
                cur.next = cur.next.next
            else:
                cur = cur.next

        return self.head

    def iter(self):
        temp = self.head
        yield temp.val
        while temp.next:
            temp = temp.next
            yield temp.val


l = ListNode(1)
a = l
a.next = ListNode(1)
a = a.next
a.next = ListNode(2)
a = a.next
a.next = ListNode(3)


s = Solution()
s.deleteDuplicates(l)
for node in s.iter():
    print(node)
