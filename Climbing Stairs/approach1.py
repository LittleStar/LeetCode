n = 5


class Solution(object):
    def climbStairs(self, n):
        """
        :type n: int
        :rtype: int
        """

        # Approach 1
        # fibs = [0, 1]
        # for i in range(n):
        #     fibs.append(fibs[-2] + fibs[-1])
        # return fibs[-1]

        # Approach 2
        # a, b = 0, 1
        # for i in range(n):
        #     a, b = b, a+b
        # return b

        # Approach 3
        # fibs = [0, 1]
        # for i in range(n):
        #     if n == 1:
        #         return 1
        #     else:
        #         return climbStairs(fibs[n-1] + fibs[n-2])


sol = Solution()
result = sol.climbStairs(n)
print(result)
