from datetime import timedelta
from enum import Enum


class Schedule():
    def __init__(self, startdate, enddate):
        self.startdate = startdate
        self.enddate = enddate
        self.calendar = {}
        self.calendar[startdate] = None
        date = startdate
        one = timedelta(days=1)
        while date <= enddate:
            self.calendar[date] = None
            date += one


class Status(Enum):
    ATTEND_CLASS = 'Attend Class'
    SELF_STUDY = 'Self Study'
    REST = 'Rest'


from schedule import Schedule, Status
from datetime import date, timedelta
import calendar


def assign_arrangements(schedule):
    index = schedule.startdate
    iterator = haveclass_or_rest()
    while index <= schedule.enddate:
        schedule.calendar[index] = next(iterator)
        index += timedelta(days=1)


def assign_special_arrangements(schedule, startdate):
    # Used for future.
    pass


def haveclass_or_rest():
    while True:
        yield Status.ATTEND_CLASS
        yield Status.ATTEND_CLASS
        yield Status.SELF_STUDY
        yield Status.ATTEND_CLASS
        yield Status.ATTEND_CLASS
        yield Status.REST


if __name__ == '__main__':
    start = date(2017, 10, 31)
    end = date(2018, 5, 1)
    course = Schedule(start, end)
    assign_arrangements(course)

    for everyday in sorted(course.calendar.items()):
        # print(calendar.weekday(everyday[0].year, everyday[0].month, everyday[0].day))
        if calendar.weekday(everyday[0].year, everyday[0].month, everyday[0].day) in [5, 6]:
            print(everyday[0], '   !!   ', everyday[1])
        else:
            print(everyday[0], '        ', everyday[1])
